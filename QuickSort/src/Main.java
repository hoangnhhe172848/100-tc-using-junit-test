
import java.util.Arrays;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Main {
     private static int[] RandArray(int n) {
        int[] randArr = new int[n];
        int i;
        int len=randArr.length;
        Random random = new Random();
        for (i = 0; i < len; i++) {
            randArr[i] = random.nextInt(10);
        }

        System.out.print("Unsorted array: ");
        System.out.println(Arrays.toString(randArr));    
        return randArr;
    }
    public static int partition(int[]a, int l, int r){
        // i tim kiem nhung phan tu lon hon hoac bang phan tu o giua
        int pivot = a[(l + r)/2], i = l, j = r;
        while(i <= j){
            while(a[i] < pivot)
                i++;
            while(a[j] > pivot)
                j--;
            if( i <= j){
                int temp = a[i];
                a[i] = a[j];
                a[j]= temp;
                i++;
                j--;
            }
        }
        return i;
    }
public static void quickSort(int []a, int l, int r){
    int i = partition(a, l ,r);
    if(l < i - 1)
        quickSort(a, l, i - 1);
    if(i < r)
        quickSort(a, i ,r);
}
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     int n = ultis.validate.inputSizeOfArray();
    int[] a = RandArray(n);  
        quickSort(a, 0, a.length-1);
    System.out.print("Sorted array: ");
    System.out.println(Arrays.toString(a));    
//        for (int i = 0; i < a.length; i++) 
//            System.out.print(a[i] + " ");
        
    }
}
