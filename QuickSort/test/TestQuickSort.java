/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestQuickSort {
    
    public TestQuickSort() {
    }
    @Test
    public void TestDefaultQuickSort(){
        int[]arr = {8, 1, 2, 4, 6, 3, 0};
        int[]expected = {0, 1, 2, 3, 4, 6, 8};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWithOddNumber(){
        int[]arr = {9, 3, 5, 1, 7};
        int[]expected = {1, 3, 5, 7, 9};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    } 
    @Test
    public void TestQuickSortWithEvenNumber(){
        int[]arr = {8, 2, 6, 4, 0, 10};
        int[]expected = {0, 2, 4, 6, 8, 10};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWithBinaryNumber(){
        int[]arr = {0010, 001, 1010, 1110, 1111};
        int[]expected = {001, 0010, 1010, 1110, 1111};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWith2NumberCharacter(){
        int[]arr = {99, 12, 33, 45, 60, 59};
        int[]expected = {12, 33, 45, 59, 60, 99};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWithNegativeNumber(){
        int[]arr = {9, -6, -2, 0, 5, 1, 2};
        int[]expected = {-6, -2, 0, 1, 2, 5, 9};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWithAllNegativeNumber(){
        int[]arr = {-9, -1, -2, -4, -1, -3};
        int[]expected = {-9, -4, -3, -2, -1, -1};
        Main.quickSort(arr, 0, arr.length-1);
    }
    @Test
    public void TestQuickSortWithAllNegative2NumberCharacter(){
        int[]arr = {-11, -99, -56, -87, -88, -10};
        int[]expected = {-99, -88, -87, -56, -11, -10};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWith4AndMoreNumberCharacter(){
        int[]arr = {999999999, 823465, 6162536, 9861235, 8653452};
        int[]expected = {823465, 6162536, 8653452, 9861235, 999999999};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWith1Element(){
        int[]arr = {5};
        int[]expected = {5};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWith2or3Element(){
        int[]arr = {99, 5};
        int[]expected = {5, 99};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestQuickSortWith4orMoreCharacterAllNegative(){
        int[]arr = {-999999999, -864635, -928383289, -625253};
        int[]expected = {-999999999, -928383289, -864635, -625253};
        Main.quickSort(arr, 0, arr.length-1);
        assertArrayEquals(expected, arr);
    }
}
// 12 test cases
