package SearchWord;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import ultis.validate;

/**
 *
 * @author farzeen
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        validate v = new validate();// goi class validate 
        Scanner sc=new Scanner(System.in);
        File input=new File(v.getString());// khoi tao mot file moi ten la input, lay du lieu nhap vao bang cach goi ham getString o class validate      
        FileReader fr=null;// khai bao fr co value = null       
        String s,str = "" ;
        if(input.exists()){// neu file duoc nhap vao co ton tai thi thuc hien hanh dong trong if
        System.out.print("Enter word you want to Search from file :");// nhap vao tu muon tim
        s=sc.nextLine();       
           System.out.println("Result is : ");
        try{// try catch bat loi 
           fr=new FileReader(input);// doc file co ten vua duoc nhap vao
           BufferedReader br=new BufferedReader(fr);           
           
           do{
            
           if(str.contains(s))// neu co chua tu duoc nhap vao thi in ra
          System.out.println(str);
           
           }while((str=br.readLine())!=null); // chay vong while de doc 
        }
        catch (IOException ex) {// bat cac loi lien quan toi doc hoac ghi           
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }            
        
        
        finally{            
                try {
                    fr.close();
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
               
            }
        }else{// neu file k ton tai in ra cau lenh duoi else
            System.out.println("File doesn't exist. Please check again");
}
}
}
    
            