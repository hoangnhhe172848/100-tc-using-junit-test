/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import fibonaci.Main;
import org.junit.Test;
import static org.junit.Assert.*;
import sun.net.www.protocol.mailto.MailToURLConnection;

/**
 *
 * @author Acer
 */
public class TestFibo {
    
    public TestFibo() {
    }
    @Test
    public void testFibo(){
        int expected = 8;
        int actual;
        actual = Main.fibo(6);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithNumber1(){
        int expected = 1;
        int actual;
        actual = Main.fibo(1);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWith2NumberCharacter(){
        int expected = 55;
        int actual;
        actual = Main.fibo(10);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithSameNumber(){
        int expected = 89;
        int actual;
        actual = Main.fibo(11);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithEvenNumber(){
        int expected = 21;
        int actual;
        actual = Main.fibo(8);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithOddNumber(){
        int expected = 34;
        int actual;
        actual = Main.fibo(9);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithSameOddNumberCharacter(){
        int expected = 89;
        int actual;
        actual = Main.fibo(11);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithSameEvenNumberCharacter(){
        int expected = 17711;
        int actual;
        actual = Main.fibo(22);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithPrimaryNumber(){
        int expected = 3;
        int actual;
        actual = Main.fibo(4);
        assertEquals(expected, actual);
    }
    @Test
    public void testFiboWithOddAndEvenNumber(){
        int expected  = 144;
        int actual;
        actual = Main.fibo(12);
        assertEquals(expected, actual);
    }
}
//10 test cases

