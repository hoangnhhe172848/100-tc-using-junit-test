package fibonaci;


import validation.validate;
import java.util.Scanner;




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Main {
static Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
    public static int fibo(int n){
        if(n < 2) return n;
        return fibo(n - 1 ) + fibo (n - 2);
        // f(0) = 0 f(1) = 1
        // f(n) = f(n -1) + f(n -2)
    }
    public static void main(String[] args) {
        int n = validate.getInt("Input number : ");// nhập vào n bằng cách gọi hàm getInt của class validate
        System.out.println("There is " + n + " " + "fibonaci number :");
        for (int i = 0; i <= n; i++) {
            
           if(i == n){
               System.out.print(fibo(i) +" ");// nếu i = n in ra số fibo và cộng với khoảng trống
           }else{
               System.out.print(fibo(i) + ",");// ngược lại nếu không bằng sẽ in ra số fibo cộng thêm dấu phẩy
           }
        }
        System.out.println(" ");
    }
}
   