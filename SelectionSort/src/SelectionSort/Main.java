package SelectionSort;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class Main{
    private static final Scanner in = new Scanner(System.in);
    private static int[] RandArray(int n) {
        int[] randArr = new int[n];
        int i;
        int len=randArr.length;
        Random random = new Random();
        for (i = 0; i < len; i++) {
            randArr[i] = random.nextInt(10);
        }

        System.out.print("Unsorted array: ");
        System.out.println(Arrays.toString(randArr));    
        return randArr;
    }
    public static void sortArrayBySelectionSort(int[] a) {
        int i,j,lowindex,tmp;        
        
        for(i=0;i<a.length-1;i++){
            
        //tim phan tu nho nhat trong mang chua duoc sap xep
        // {34,99,8,25,1} phan tu nho nhat la 1
            lowindex=i;
            for(j=i+1;j<a.length;j++){
                if(a[j]<a[lowindex]){
                    lowindex=j;
                } 
            }
        // hoan doi phan tu nho  nhat va phan tu dau tien(hay phan tu dung o vi tri i)
//sau khi hoan doi se duoc { 1,99,8,25,34}
                tmp=a[lowindex];
                a[lowindex]=a[i];
                a[i]=tmp;
        }
    System.out.print("Sorted array: ");
    System.out.println(Arrays.toString(a));    
    }    
    public static void main(String[] args) {
    int n = validation.validate.inputSizeOfArray();
    int[] a = RandArray(n);
    sortArrayBySelectionSort(a);
    } 
}