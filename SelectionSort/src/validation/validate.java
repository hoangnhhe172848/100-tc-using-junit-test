/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validation;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class validate {
    static Scanner sc = new Scanner(System.in);
    public static int checkInputInt() {
        //loop until user input correct
        while (true) {
                    System.out.print("Enter number of array: ");
            try {
                int result = Integer.parseInt(sc.nextLine().trim());
                return result;
            } catch (Exception e) {
                System.out.println("Input can't be empty and must be number. Please input again : ");
            }
        }
    }
    public static int inputSizeOfArray() {
        int n = checkInputInt();
        return n;
    }
}
