/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import SelectionSort.Main;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestSelectionSort {

    public TestSelectionSort() {
    }

    @Test
    public void testSelectionSort() {
        int[] arr = {6, 4, 3, 2, 8};
        int[] expected = {2, 3, 4, 6, 8};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWith2Element() {
        int[] arr = {8, 1};
        int[] expected = {1, 8};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWithEvenNumber() {
        int[] arr = {10, 4, 2, 8, 6, 0};
        int[] expected = {0, 2, 4, 6, 8, 10};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWithOddNumber() {
        int[] arr = {11, 3, 5, 1, 7, 9};
        int[] expected = {1, 3, 5, 7, 9, 11};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWith2NumberCharacter() {
        int[] arr = {54, 12, 87, 99, 10};
        int[] expected = {10, 12, 54, 87, 99};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWith3AndBigNumberCharacter() {
        int[] arr = {123, 987, 19234, 100, 100987};
        int[] expected = {100, 123, 987, 19234, 100987};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void testSelectionSortWithBinaryNumber() {
        int[] arr = {1010, 0010, 0101, 1111, 1011};
        int[] expected = {0010, 0101, 1010, 1011, 1111};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void testSelectionSortWithNegativeNumber(){
        int[]arr = {-1, -5, 0, -10, 1, -20};
        int[]expected = {-20, -10, -5, -1, 0, 1};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void testSelectionSortAllNegativeNumber(){
        int[]arr = {-1, -10, -5, -2, -8};
        int[]expected = {-10, -8, -5, -2, -1};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void testSelectionSortWithBigNegativeNumber(){
        int[]arr = {-120, -200, -189, -10000, -99999};
        int[]expected = {-99999, -10000, -200, -189, -120};
        Main.sortArrayBySelectionSort(arr);
        assertArrayEquals(expected, arr);
    }
}
//10 test cases
