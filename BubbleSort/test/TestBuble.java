/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import bublesort.Main;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestBuble {

    public TestBuble() {
    }

    @Test
    public void TestBubbleWithChuoi() {
        int[] arr1 = {5, 4, 6, 1, 8, 2, 3};
        int[] expectedoutput = {1, 2, 3, 4, 5, 6, 8};
        Main.bubleSort(arr1);
        assertArrayEquals(expectedoutput, arr1);
    }

    @Test
    public void TestBubbleWithNegativeNumber() {
        int[] arr1 = {1, 3, 2, 4, -1, 0, -3, 6};
        int[] expectedoutput = {-3, -1, 0, 1, 2, 3, 4, 6};
        Main.bubleSort(arr1);
        assertArrayEquals(expectedoutput, arr1);
    }

    @Test
    public void TestWithAllNegativeNumber() {
        int[] arr = {-1, -4, -2, -3, -6};
        int[] expected = {-6, -4, -3, -2, -1};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestWithEvenNumber() {
        int[] arr = {0, 4, 2, 8, 6, 10};
        int[] expected = {0, 2, 4, 6, 8, 10};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestWithOddNumber() {
        int[] arr = {9, 3, 1, 5, 7};
        int[] expected = {1, 3, 5, 7, 9};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestWithNumberZero() {
        int[] arr = {0};
        int[] expected = {0};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestVoi4ChuSo() {
        int[] arr = {1011, 9292, 1023, 1045, 1000};
        int[] expected = {1000, 1011, 1023, 1045, 9292};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestVoi5ChuSoHoacNhieuHon() {
        int[] arr = {12302, 102309, 1402, 1293736, 190, 3};
        int[] expected = {3, 190, 1402, 12302, 102309, 1293736};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void TestVoiSoNhiPhan() {
        int[] a = {0010, 1010, 1111, 1011, 101};
        int[] expected = {0010, 101, 1010, 1011, 1111};
        Main.bubleSort(a);
        assertArrayEquals(expected, a);
    }
    @Test
    public void TestEmptyInput(){
        int[]arr = {};
        int[]expected = {};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestWith1Element(){
        int[]arr = {5};
        int[]expected = {5};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }
    @Test
    public void TestWith2Element(){
        int[]arr = {99, 4};
        int[]expected = {4, 99};
        Main.bubleSort(arr);
        assertArrayEquals(expected, arr);
    }
}
//12 test cases
