package bublesort;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import validation.validate;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class Main {

    
    validate v = new validate();
static Scanner sc = new Scanner(System.in);
    /**
     * @param args the command line arguments
     */
private static int[] RandArray(int n) {
        int[] randArr = new int[n];
        int i;
        int len=randArr.length;
        Random random = new Random();
        for (i = 0; i < len; i++) {
            randArr[i] = random.nextInt(10);
        }

        System.out.print("Unsorted array: ");
        System.out.println(Arrays.toString(randArr));    
        return randArr;
    }
  public static void bubleSort(int [] a){
        for (int i = 0; i < a.length - 1; i++) {
            boolean check = true;
            for (int j = 0; j < a.length - 1; j++) {
                if(a[j] > a[j + 1]){
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                    check = false;   
                }// sau vòng for đầu tiên số lớn nhất được đưa xuống cuối( VD : 0, 9 , 6 , 5) => 0, 6, 5, 9.)
            }
            if(check )// tạo ra biến check ở trên để check nếu 5 < 9 ( true ) thì sẽ không cần phải so sánh nữa mà return luôn )
                return;
        }
          
    }
    public static void main(String[] args) {
        int n = validate.getInt("Input array size : ");// nhập vào n bằng cách gọi ra hàm getInt ở class validate
         int[] a = RandArray(n);
 bubleSort(a);
 System.out.print("Sorted array: ");
    System.out.println(Arrays.toString(a));    
    }
}
