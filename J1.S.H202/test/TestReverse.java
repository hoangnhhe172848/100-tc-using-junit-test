/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestReverse {

    public TestReverse() {
    }

    @Test
    public void testReverse() {
        String expected = "olleh";
        String actual;
        actual = Main.printReverse("hello");
        assertEquals(expected, actual);
    }

    @Test
    public void testReverseWithUpperCase() {
        String expected = "OLLEH";
        String actual;
        actual = Main.printReverse("HELLO");
        assertEquals(expected, actual);
    }

    @Test
    public void testReverseWithLowerCase() {
        String expected = "ekin";
        String actual;
        actual = Main.printReverse("nike");
        assertEquals(expected, actual);
    }

    @Test
    public void testReverseWithNumber() {
        String expected = "0991";
        String actual;
        actual = Main.printReverse("1990");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithAllCharacter() {
        String expected = "9olleh";
        String actual;
        actual = Main.printReverse("hello9");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithSpecialCharacter() {
        String expected = "@ognib";
        String actual;
        actual = Main.printReverse("bingo@");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithAllSpecialCharacter() {
        String expected = "@$#&%$";
        String actual;
        actual = Main.printReverse("$%&#$@");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithLongCharacter() {
        String expected = "eman ym olleh";
        String actual;
        actual = Main.printReverse("hello my name");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithNull() {
        String expected = "";
        String actual;
        actual = Main.printReverse("");
        assertEquals(expected, actual);
    }

    @Test
    public void testReverseWith2Character() {
        String expected = "ba";
        String actual;
        actual = Main.printReverse("ab");
        assertEquals(expected, actual);
    }

    @Test
    public void testReverseAllCharacter() {
        String expected = "123@bca";
        String actual;
        actual = Main.printReverse("acb@321");
        assertEquals(expected, actual);

    }

    @Test
    public void testReverseWithNumberAndSpecialCharacter() {
        String expected = "321@654";
        String actual;
        actual = Main.printReverse("456@123");
        assertEquals(expected, actual);

    }
}
//12 test cases
