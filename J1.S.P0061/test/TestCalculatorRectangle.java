/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import J1.S.P0061.J1SP0061;
import J1.S.P0061.Rectangle;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestCalculatorRectangle extends TestCase{
    
    public TestCalculatorRectangle() {
    }
    public void testContructors(){
        new Rectangle(4.5, 3.4);
    }
    public void testPerimeter(){
        Rectangle r1 = new Rectangle(4.5, 3.2);
        Rectangle r2 = new Rectangle(2.2, 1.4);
        Rectangle r3 = new Rectangle(1, 2.4);
        Rectangle r4 = new Rectangle(0, 1.3);
        
        assertEquals(r1.getPerimeter(), 15.4);
        assertEquals(r2.getPerimeter(), 7.2);
        assertEquals(r3.getPerimeter(), 6.8);
        assertEquals(r4.getPerimeter(), 2.6);
    }
    public void testArea(){
        Rectangle r1 = new Rectangle(4, 3.2);
        Rectangle r2 = new Rectangle(1.2, 1.4);
        Rectangle r3 = new Rectangle(2, 2.4);
        Rectangle r4 = new Rectangle(0, 1.3);
        
        assertEquals(r1.getArea(), 12.8);
        assertEquals(r2.getArea(), 1.68);
        assertEquals(r3.getArea(), 4.8);
        assertEquals(r4.getArea(), 0.0);
    }
    public void testPerimeterWith2NumberCharacter(){
        Rectangle r = new Rectangle(11, 88);
        assertEquals(r.getPerimeter(), 198.0);
    }
    public void testPerimeterWith3NumberChacrecter(){
        Rectangle r = new Rectangle(111, 888);
        assertEquals(r.getPerimeter(), 1998.0);
    }
    public void testPerimeterWithBigNumberCharacter(){
        Rectangle r = new Rectangle(10000, 800000);
        assertEquals(r.getPerimeter(), 1620000.0);
    }
    public void testPerimeterWith0(){
        Rectangle r = new Rectangle(0,0);
        assertEquals(r.getPerimeter(), 0.0);
    }
    public void testAreaWith2NumberCharacter(){
        Rectangle r = new Rectangle(11, 99);
        assertEquals(r.getArea(), 1089.0);
    }
    public void testAreaWith3NumberCharacter(){
        Rectangle r = new Rectangle(111, 999);
        assertEquals(r.getArea(), 110889.0);
    }
    public void testAreaWithBigNumberCharacter(){
        Rectangle r = new Rectangle(100000, 8000);
        assertEquals(r.getArea(), 800000000.0);
    }
    public void testAreaWith0(){
        Rectangle r = new Rectangle(0, 0);
        assertEquals(r.getArea(), 0.0);
    }
}
//11 test cases

