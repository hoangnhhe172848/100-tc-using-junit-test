/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import J1.S.P0061.Triangle;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestCalculatorTriangle extends TestCase{
    
    public TestCalculatorTriangle() {
    }
    public void testContructors(){
        new Triangle(4.5, 3.1, 1.2);
    }
    public void testPerimeter(){
        Triangle t1 = new Triangle(3.4, 2.4, 1.5);
        Triangle t2 = new Triangle(2.2, 1.4, 1.1);
        Triangle t3 = new Triangle(1, 2.4, 1.2);
        Triangle t4 = new Triangle(0, 1.3, 1.3);
        
        assertEquals(t1.getPerimeter(), 7.3);
        assertEquals(t2.getPerimeter(), 4.7);
        assertEquals(t3.getPerimeter(), 4.6);
        assertEquals(t4.getPerimeter(), 2.6);
    }
    public void testArea(){
        Triangle t1 = new Triangle(3, 4, 5);
        Triangle t2 = new Triangle(6, 8, 10);
        
        assertEquals(t1.getArea(), 6.0);
        assertEquals(t2.getArea(), 24.0);

    }
    public void testPerimeterWith0(){
    Triangle t = new Triangle(0, 0, 0);
        assertEquals(t.getPerimeter(), 0.0);
}
    public void testPerimeterWith2NumberCharacter(){
        Triangle t = new Triangle(11, 22, 43);
        assertEquals(t.getPerimeter(), 76.0);
    }
    public void testPerimeterWith3NumberCharacter(){
        Triangle t = new Triangle(111, 212, 980);
        assertEquals(t.getPerimeter(), 1303.0);
    }
    public void testPerimeterWithBigNumberCharacter(){
        Triangle t = new Triangle(10000, 80000, 900000);
        assertEquals(t.getPerimeter(), 990000.0);
    }
    public void testAreaWith0(){
        Triangle t = new Triangle(0, 0, 0);
        assertEquals(t.getArea(), 0.0);
    }
    public void testAreaWith2NumberCharacter(){
        Triangle t = new Triangle(10, 10, 12);
        assertEquals(t.getArea(), 48.0);
    }
    public void testAreaWith3NumberCharacter(){
        Triangle t = new Triangle(84, 96, 120);
        assertEquals(t.getArea(), 4004.747183031658);
    }
}
//10 test cases
