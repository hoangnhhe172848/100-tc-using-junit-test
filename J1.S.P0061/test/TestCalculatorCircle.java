/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import J1.S.P0061.Circle;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Acer
 */
public class TestCalculatorCircle extends TestCase{
    
    public TestCalculatorCircle() {
    }
    public void testContructor(){
       new Circle(4.5);
    }
    public void testPerimeter(){
        Circle r1 = new Circle(4);
        Circle r2 = new Circle(2.2);
        Circle r3 = new Circle(1);
        
        assertEquals(r1.getPerimeter(), 25.0);
        assertEquals(r2.getPerimeter(), 14.0);
        assertEquals(r3.getPerimeter(), 6.0);
    }
    public void testArea(){
        Circle r1 = new Circle(4);
        Circle r2 = new Circle(1.4);
        Circle r3 = new Circle(2.4);
        
        assertEquals(r1.getArea(), 50.0);
        assertEquals(r2.getArea(), 6.0);
        assertEquals(r3.getArea(), 18.0);
    }
    public void testPerimeterWith2NumberCharacter(){
        Circle r = new Circle(45);
        assertEquals(r.getPerimeter(), 283.0);
    }
    public void testAreaWith2NumberCharacter(){
        Circle r = new Circle(56);
        assertEquals(r.getArea(), 9847.0);
    }
    public void testPerimeterWith3NumberCharacter(){
        Circle r = new Circle(189);
        assertEquals(r.getPerimeter(), 1187.0);
    }
    public void testPerimeterWithBigNumberCharacter(){
        Circle r = new Circle(800234);
        assertEquals(r.getPerimeter(), 5025470.0);
    }
    public void testAreaWith3NumberCharacter(){
        Circle r = new Circle(180);
        assertEquals(r.getArea(), 101736.0);
    }
    public void testAreaWithBigNumberCharacter(){
        Circle r = new Circle(10000);
        assertEquals(r.getArea(), 314000000.0);
    }
    public void testPerimeterWith0(){
        Circle r = new Circle(0);
        assertEquals(r.getPerimeter(), 0.0);
    }
    public void testAreaWith0(){
        Circle r = new Circle(0);
        assertEquals(r.getArea(), 0.0);
    }
 }
//11 test case
