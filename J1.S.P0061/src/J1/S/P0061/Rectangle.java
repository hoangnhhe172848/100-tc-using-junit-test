/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package J1.S.P0061;

/**
 *
 * @author Acer
 */
public class Rectangle extends Shape{
    double width;
    double length;

    public Rectangle() {
    }

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }  

    @Override
            // tính chu vi hình chu nhat
   public double getPerimeter() {
        // tra ve gia tri chu vi hình chu nhat
        return 2 * (width + length);
    }

    @Override
            // tính diện tích hình chu nhat
   public double getArea() {
       return width * length;
    }

    @Override
            // in ra kết quả
    void printResult() {
        System.out.println("Width : " + width);
        System.out.println("Length : " + length);
        System.out.println("Area : " + getArea());
        System.out.println("Perimeter : " + getPerimeter());
    }
}
