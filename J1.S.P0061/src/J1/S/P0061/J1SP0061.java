package J1.S.P0061;

import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class J1SP0061 {

    /**
     * @param args the command line arguments
     */
    static Scanner sc = new Scanner(System.in);
    public double checkInputDouble(){
        do {            
            try {
                double result = Double.parseDouble(sc.nextLine());
                return result;
            } catch (Exception e) {
                System.out.println("Wrong input. Input cant be empty");
            }
        } while (true);
    }
    public Rectangle inputRectangle(){
        while (true) {            
            System.out.println("Please input witdh of Ractangle : ");
            double c = checkInputDouble();
            System.out.println("Please input length of Rectangle : ");
            double b = checkInputDouble();
            // check điều kiện chiều rộng phải bé hơn chiều dài
            if(c <= b){
                // trả về giá trị tam giác có chiều rộng và dài lần lượt là c và b
                return new Rectangle(b, b);
            }
            else{
                // nếu không báo lỗi
                System.err.println("RE_INPUT");
            }
        }
    }
    public Circle inputCircle(){
        while (true) {            
            System.out.println("Input radius of Circle : ");
            double r = checkInputDouble();
            // check điều kiện bán kính hình tròn phải > 0 
            if(r > 0){
                // trả về giá trị hình tròn có bán kính r
                return new Circle(r);
            }else{
                System.err.println("RE-INPUT");
            }
        }
    }
    public Triangle inputTriangle(){
        while (true) {            
            System.out.println("Please input side A of Triangle : ");
            double a = checkInputDouble();
            System.out.println("Please input side B of Triangle : ");
            double b = checkInputDouble();
            System.out.println("Please input side C of Triangle : ");
            double c = checkInputDouble();
            // check điều kiện của tam giacs đó là tổng hai cạnh bất kì phải lớn hơn cạnh còn lại
            if(a + b > c && a + c > b && b + c > a){
                // trả về giá trị hình tam giác có 3 cạnh a , b ,c
                return new Triangle(a, b, c);
            }else{
                System.err.println("RE-INPUT");
            }
        }
    }
    public void Display(Circle circle, Rectangle rectangle, Triangle triangle){
        System.out.println("-------Rectangle-------");
        rectangle.printResult();
        System.out.println("------CIrcle------");
        circle.printResult();
        System.out.println("------Triangle-------");
        triangle.printResult();
    }
    public static void main(String[] args) {
       J1SP0061 j1 = new J1SP0061();
       Rectangle rec = j1.inputRectangle();
       Circle cir = j1.inputCircle();
       Triangle tri = j1.inputTriangle();
       j1.Display(cir, rec, tri);
    }
    
}
