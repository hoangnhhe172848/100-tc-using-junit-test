/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package J1.S.P0061;

/**
 *
 * @author Acer
 */
public class Circle extends Shape{
double r;

    public Circle() {
    }

    public Circle(double r) {
        this.r = r;
    }

    @Override
            // tinh chu vi hinh tron 
    public double getPerimeter() {
        // math.pi là một hằng số kiểu double trong Math class chứa giá trị của hằng số PI trong toán học
        return Math.round(3.14 * 2 *r);
    }

    @Override
            // tính diện tích hình tròn
    public double getArea() {
        return Math.round(3.14*r*r);
    }

    @Override
            // in ra kết quả
    void printResult() {
        System.out.println("Radius : " + this.r);
        System.out.println("Area : " + getArea());
        System.out.println("Perimeter : " + getPerimeter());
    }
    
}
