/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package J1.S.P0061;

/**
 *
 * @author Acer
 */
public class Triangle extends Shape{
    double a;
    double b;
    double c;

    public Triangle() {
    }

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
            // tính chu vi tam giác
    public double getPerimeter() {
        return a + b + c;
    }

    @Override
            // tính diện tích tam giác
    public double getArea() {
       double p = (a + b + c)/2;
       // trả về giá trị căn bậc 2....
       return Math.sqrt(p*(p - a) *(p - b) * (p-c));
    }

    @Override
            // in ra kết quả
    void printResult() {
        System.out.println("Side A : " + this.a);
        System.out.println("Side A : " + this.b);
        System.out.println("SIde A : " + this.a);
        System.out.println("Area : " + getArea());
        System.out.println("Perimeter : " + getPerimeter());
    }
    
}
