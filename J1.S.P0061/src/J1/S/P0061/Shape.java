/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package J1.S.P0061;

/**
 *
 * @author Acer
 */
// tao 1 class abstract theo de bai cho
public abstract class Shape {
    abstract double getPerimeter();
    abstract double getArea();
    abstract void printResult();
}
