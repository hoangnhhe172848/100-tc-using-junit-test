/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;
import sun.applet.Main;

/**
 *
 * @author Acer
 */
public class TestReverseNumber {
    
    public TestReverseNumber() {
    }
    @Test
    public void testReverseDigitPair(){
        int expected = 32;
        int actual;
         actual = reverseNumber.swapDigitPairs(23);
         assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith2EvenNumber(){
        int expected = 24;
        int actual;
        actual = reverseNumber.swapDigitPairs(42);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith2OddNumber(){
        int expected = 35;
        int actual;
        actual = reverseNumber.swapDigitPairs(53);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWit3Number(){
        int expected = 235;
        int actual;
        actual = reverseNumber.swapDigitPairs(253);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith4Number(){
        int expected = 2345;
        int actual;
        actual = reverseNumber.swapDigitPairs(3254);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWithLongNumber(){
        int expected = 12365478;
        int actual;
        actual = reverseNumber.swapDigitPairs(21634587);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith2NegativeNumberCharacter(){
        int expected = 0;
        int actual;
        actual = reverseNumber.swapDigitPairs(-32);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith2SameNumber(){
        int expected = 55;
        int actual;
        actual = reverseNumber.swapDigitPairs(55);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseDigitPairWith2SameNumber3Character(){
        int expected = 121;
        int  actual;
        actual = reverseNumber.swapDigitPairs(112);
        assertEquals(expected, actual);
    }
    @Test
    public void testReverseWith0(){
        int expected = 0;
        int actual;
        actual = reverseNumber.swapDigitPairs(0);
        assertEquals(expected, actual);
    }
}
//10 test cases
