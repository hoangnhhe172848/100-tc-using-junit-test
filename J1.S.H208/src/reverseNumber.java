/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class reverseNumber {

   public static int swapDigitPairs(int n) {
    int result = 0;
    int m = 1;
    // chay neu n > 0
    while(n > 0) {
        int vt1 = n % 10;
        n /= 10;
        // neu n = 0
        if(n == 0) {
            result += m * vt1;
            break;
        }
        // doi 2 vi tri d1 va d2 bang cach nhan he 10 va he 1
        int vt2 = n % 10;
        result = result + m * 10 * vt1 + m * vt2;
        // tiep tuc chia n cho 10
        n /= 10;
        // sau moi 2 lan doi cho thi vi tri thu3 cach vi tri dau 100 don vi
        m *= 100;
    }
    return result;
}
     public static void main(String[] args) {
         System.out.println(swapDigitPairs(-23));
    }
}
